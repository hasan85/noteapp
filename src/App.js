import React from 'react';
import "./styles/main.scss"
import './App.css';
import { NoteList } from './components/noteList'
import { AddNote } from './components/AddNote'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notes: []
    }
  }

  handleSubmit = note => {
    this.setState({ notes: [...this.state.notes, note] }, () => {
      console.log("new note [app.js] -> ", note);
      console.log(" notes [app.js] -> ", this.state.notes);
    });
  }

  render() {
    const { notes } = this.state;
    return (
      <div className="App d-flex justify-content-center align-items-center">
        <AddNote handleSubmit={this.handleSubmit} />
        <NoteList _notes={notes}/>
      </div>
    );
  }
}

export default App;
