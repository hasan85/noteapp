import React, { Component } from 'react';
import Note from "./Note"


export class NoteList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            notes: []
        }
    }

    // static getDerivedStateFromProps (props, state) {
    //     debugger;
    //     if (props.notes !== state.notes) {
    //         return {
    //             notes: props.notes
    //         };
    //     }
    //     return null;
    // }
    componentWillReceiveProps = (nextProps) => {
        this.setState({
            notes: nextProps._notes
        })
    }

    render() {
        console.log("notes => ", this.state.notes);
        return (
            <div className="text-right">
                {this.state.notes.map((note, k) => (
                    <Note key={k} note={note} />
                ))}
            </div>
        );
    }

}

// export default NoteList;