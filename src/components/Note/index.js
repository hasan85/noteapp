import React, { Component } from "react";
export default class Note extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            content: "",
            rating: 0
        };
    }

    render() {
        const { title, content, rating } = this.props.note;
        console.log(this.props);
        return (
            <div>
                <p>{title}</p>
                <p>{content}</p>
                <p>{rating}</p>
            </div>
        );
    }
}