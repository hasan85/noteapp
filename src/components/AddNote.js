import React, { Component } from "react";


export class AddNote extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            content: "",
            rating: 0
        };
    }
    handleChange = e =>
        this.setState({ [e.target.name]: e.target.value });

    handleSubmit = e => {
        e.preventDefault();
        this.props.handleSubmit({...this.state });

    }
    render() {
        const { title, content, rating } = this.state;
        return (
            <div className="add-note-card">
                <form action="/form-submit" onSubmit={this.handleSubmit} className="d-flex flex-column justify-content-center align-items-center">
                    <input placeholder="title" type="text" name="title" className="note-title" value={title} onChange={this.handleChange} />
                    <input placeholder="content" type="text" name="content" className="note-content" onChange={this.handleChange} />
                    <input placeholder="rating" type="number" name="rating" className="note-rating" onChange={this.handleChange} />
                    <input type="submit" value="ADD" />
                </form>
            </div>
        );
    }
} 